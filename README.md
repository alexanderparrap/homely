# Exercise Description

> _Version **106a010**_

The following code test was created to evaluate your abilities as an Android Engineer. Some of the areas that you'll be evaluated in are:

- Creativity
- Proactivity
- Analytical skills
- Code quality
- Organization
- Ability to follow instructions
- Attention to details

Please treat this project and the tasks below as if they were part of, or the beginning of a bigger application.

We would like you to solve each task as they appear below, and avoid doing early optimizations or refactoring unless specified or if it blocks your solution for a specific problem. Unless stated on the task please prevent from using any third party library. If any code is copied from the internet, make sure to add a comment with a reference to where you sourced it from.

As you work on each task, please prefix your commit(s) message(s) with the task reference using the following pattern:

> [Task-XX] My commit message

When you complete a task, as part of your commit and changes, please change the task's status from `Pending` to `Completed` and add any comments into the designated area at the bottom of each task, where you can explain what problems you found and how you solved them. You can also explain how you'd do something more complex and/or time consuming if you had the time for it, feel free to also add any other comments that are relevant to the task.

# Tasks

---

## `Task-01`: Is there a better way to load the contents JSON file?

_Status: `Complete`_

The information displayed on the `Listings` tab comes from a file stored in the project in JSON format. If there is a better way to load the contents of the file, please do so. Also, feel free to make the code more MVVM like in the process.

```
I simplified the Json reading function and also I relocated this function in the data layout.
```

---

## `Task-02`: Remove duplicate listings

_Status: `Complete`_

Unfortunately in the process of exporting the list of listings contained on the file referenced in the preceding task, duplicate listings were added to the final list. Without modifying the contents of the JSON file, and preserving the order of the list, prevent the app from showing the duplicate listings.

Please write your own function as we would like to see your developer skills in this task.

```
I added this function in listingsUseCase in order to unit test this method later on 
```

---

## `Task-03`: Can you make any improvements to ListingsListViewAdapter?

_Status: `Complete`_

If there is anything to be improved on that class, please do so.

```
I replaced this adapter for RecyclerView because Listview has performant issues and lack of flexibility according with google documentation. Also this ui component is not longer maintained for google. 

```

---

## `Task-04`: Is `ListView` the correct widget

_Status: `Complete`_

We are using a `ListView` to display the listings information. On future tasks you will be asked to update the look and feel of each element. Based on the code you have seen and on the future requirements of the following tasks, feel free to replace the existing widget if appropriate.

Please comment below why you decided to change or to keep the widget.

```
the recycler adapter has been created to hold different holder views.
```

---

## `Task-05`: Implement listing's shortlist functionality

_Status: `Complete`_

Replace the hardcoded shortlist from `ListingsViewModel` with a proper functionality, linking the checkboxes in the listings view to this in-memory list of shortlisted properties.

Feel free to modify the `isShortlisted` function and/or the `_shortlist`/`shortlist` properties as needed.

The acceptance criteria for this task is:

1. Listings that are shortlisted should have the checkbox _checked_
2. Listings that are not shortlisted should have the checkbox _unchecked_
3. Checking an unchecked checkbox should add a listing to the shortlist
4. Unchecking a checked checkbox should remove a listing from the shortlist

There is no need to keep the default/hard coded list of shortlisted listings.

```
Add comments here
```

---

## `Task-06`: Shortlist tab

_Status: `Complete`_

On the shortlist tab, show all and only the shortlisted listings. As listings are added or removed to the shortlist they should appear or be removed on this tab.

The contents of both tabs, `Listings` and `Shortlist`, should be kept in sync.

```
Add comments here
```

---

## `Task-07`: Image Viewer tab

_Status: `Complete`_

You might have noticed that there is a bug on the `Image Viewer` tab. We would like you to make the necessary code changes to fix the bug. We would like it to look similar to the screenshot below.

![](images/ImageViewerFragment.png)

Please do not use any third party tools and do not modify the original image file while working on this task.

Once you are done, please explain on the comments section what the problem was and how you fixed it.

```
The problem was image size. It was too big that I had scale the bitmap in order to fix the issue. Also I have included this block of code inside of a background thread in order to avoid ANRs. 
```

---

## `Task-08`: Listing card

_Status: `Pending`_

Back to listing related stuff...

At the moment, the information of each listing is only in text format and quite boring. Show your UI skills by making the list look similar to the image below.

![](images/listing_cards.png)

You are not required to add any other information than the 3 lines of text already being displayed. The only new element for you to add is the first image of the listing. The rest is just styling.

For this task you can import a third party dependency for loading the image from the network.

You can add more information to the card if you prefer to do so.

In the [images folder](images/) you can find the icons that can be used for the shortlist functionality and replace the checkbox we have used so far.

![](images/Heart.svg)
![](images/Heart2.svg)

```
Add comments here
```

---

## `Task-09`: Architecture

_Status: `Complete`_

Please make any architectural changes you would like. If the changes would be too big for the scope of this exercise, please explain below what you would do to improve this project.

```
Architecture
  - data - external dependencies or local services
  - domain - This layout contains usecases and models
  - ui/presentaion - it contains UI components and viewmodels 
I have implemented clean architecture in order to make the project testable, scalable and manteinable.
```

---

## `Task-10`: Tests

_Status: `Pending`_

It is expected that you write tests for important changes you made above on the previous tasks, if any test was relevant. As a last pass, please check any code, existing or new that would benefit from tests and write them. Explain below the benefits of the tests you wrote and why they are important.

```
Add comments here
```

---

## `Task-11`: UI/UX improvements

_Status: `Pending`_

Please make any final UI/UX improvements you feel would enhance the app. If you run any profiling tools, please let us know below what your findings were and what could be further improved.

```
Add comments here
```

---

## `Task-12`: Any final comments

_Status: `Pending`_

That's it. Regardless of if you finished all the tasks above successfully or not, we would like to see your comments regarding this test or any other improvements you would do to the project. If you forgot to add any comment on the previous tasks feel free to add them here as well.

```
Add comments here
```

---
