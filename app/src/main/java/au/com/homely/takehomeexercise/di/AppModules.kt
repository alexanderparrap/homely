package au.com.homely.takehomeexercise.di

import android.content.Context
import au.com.homely.takehomeexercise.data.TakeWorkRepositoryImpl
import au.com.homely.takehomeexercise.data.factory.DataFactory
import au.com.homely.takehomeexercise.domain.repository.TakeWorkRepository
import au.com.homely.takehomeexercise.domain.usecases.ListingsUseCase
import au.com.homely.takehomeexercise.domain.usecases.ListingsUseCaseImpl
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
object ApplicationModule {

    @Singleton
    @Provides
    fun provideDataFactory(@ApplicationContext context: Context): DataFactory{
        return DataFactory(context)
    }
}

@Module
@InstallIn(SingletonComponent::class)
abstract class TakeWorkRepositoryModule {
    @Binds
    abstract fun bindTakeWorkRepository(
        takeWorkRepositoryImpl: TakeWorkRepositoryImpl
    ):TakeWorkRepository
}

@Module
@InstallIn(SingletonComponent::class)
abstract class ListingsUseCaseModule {
    @Binds
    abstract fun bindListingsUseCase(
        listingsUseCaseImpl: ListingsUseCaseImpl
    ):ListingsUseCase
}