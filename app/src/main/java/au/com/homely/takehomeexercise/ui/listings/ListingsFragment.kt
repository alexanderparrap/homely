package au.com.homely.takehomeexercise.ui.listings

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import au.com.homely.takehomeexercise.databinding.ListingsFragmentBinding
import au.com.homely.takehomeexercise.ui.SharedViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ListingsFragment : Fragment() {

    private val sharedViewModel: SharedViewModel by activityViewModels()
    private var _binding: ListingsFragmentBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = ListingsFragmentBinding.inflate(inflater, container, false)
        val adapter = ListingRecyclerViewAdapter(sharedViewModel)
        binding.listingsListview.adapter = adapter

        return binding.root
    }

}
