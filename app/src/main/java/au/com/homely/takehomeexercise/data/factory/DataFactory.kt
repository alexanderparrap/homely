package au.com.homely.takehomeexercise.data.factory

import android.content.Context
import au.com.homely.takehomeexercise.data.factory.datasources.local.LocalServices
import au.com.homely.takehomeexercise.domain.models.ListingAPIResponse
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

class DataFactory @Inject constructor(
    @ApplicationContext val context: Context
) {
    fun retrieveListings(): ListingAPIResponse? {
        return LocalServices(context).retrieveListings()
    }
}