package au.com.homely.takehomeexercise.data

import au.com.homely.takehomeexercise.data.factory.DataFactory
import au.com.homely.takehomeexercise.domain.models.ListingAPIResponse
import au.com.homely.takehomeexercise.domain.repository.TakeWorkRepository
import javax.inject.Inject

class TakeWorkRepositoryImpl @Inject constructor(
    private val dataFactory: DataFactory
) : TakeWorkRepository {
    override fun retrieveListings(): ListingAPIResponse? {
        return dataFactory.retrieveListings()
    }
}