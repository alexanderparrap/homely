package au.com.homely.takehomeexercise.ui.listings

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import au.com.homely.takehomeexercise.databinding.ImageListingsItemBinding
import au.com.homely.takehomeexercise.domain.models.Listing
import com.bumptech.glide.Glide

class ListingsImagePagerAdapter(private val images: List<Listing.Image>) : PagerAdapter() {

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val binding = ImageListingsItemBinding.inflate(LayoutInflater.from(container.context), container, false)
        Glide.with(container.context).load(images[position].uri).optionalCenterCrop().into(binding.imageView)
        container.addView(binding.root)
        return binding.root
    }

    override fun getCount(): Int = images.count()

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }
}