package au.com.homely.takehomeexercise

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class TakeHomeApp : Application() {
    override fun onCreate() {
        super.onCreate()
    }
}