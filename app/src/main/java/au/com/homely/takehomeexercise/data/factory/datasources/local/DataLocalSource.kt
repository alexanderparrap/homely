package au.com.homely.takehomeexercise.data.factory.datasources.local

import au.com.homely.takehomeexercise.domain.models.ListingAPIResponse

interface DataLocalSource {
    fun retrieveListings(): ListingAPIResponse?
}