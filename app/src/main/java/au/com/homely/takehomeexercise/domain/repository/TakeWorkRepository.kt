package au.com.homely.takehomeexercise.domain.repository

import au.com.homely.takehomeexercise.domain.models.ListingAPIResponse

interface TakeWorkRepository {
    fun retrieveListings(): ListingAPIResponse?
}