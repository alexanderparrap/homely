package au.com.homely.takehomeexercise.domain.usecases

import au.com.homely.takehomeexercise.domain.models.Listing
import au.com.homely.takehomeexercise.domain.models.ListingAPIResponse
import au.com.homely.takehomeexercise.domain.repository.TakeWorkRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Inject

interface ListingsUseCase {
    suspend fun retrieveListings():ListingAPIResponse?
    fun removeDuplicateListings(current:List<Listing>): List<Listing>
}
class ListingsUseCaseImpl @Inject constructor(
    private val repository: TakeWorkRepository
): ListingsUseCase {

    override suspend fun retrieveListings() = repository.retrieveListings()

    override fun removeDuplicateListings(current: List<Listing>): List<Listing> {
        return current.distinct()
    }
}

