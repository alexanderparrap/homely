package au.com.homely.takehomeexercise.ui.shortlist

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import au.com.homely.takehomeexercise.databinding.ListingsListItemBinding
import au.com.homely.takehomeexercise.domain.models.Listing
import au.com.homely.takehomeexercise.ui.SharedViewModel
import au.com.homely.takehomeexercise.ui.listings.ListingsImagePagerAdapter

class ShortListRecyclerViewAdapter(
    private val viewModel: SharedViewModel,
    val updateList:() -> Unit,
): RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListingsViewHolder {
        val binding = ListingsListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ListingsViewHolder(binding)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder){
            is ListingsViewHolder -> {
                val listing = viewModel.shortlist.value?.get(position)
                holder.bind(listing)
            }
        }
    }

    override fun getItemCount(): Int = viewModel.shortlist.value?.size!!

    inner class ListingsViewHolder(private val binding: ListingsListItemBinding): RecyclerView.ViewHolder(binding.root){
        fun bind(listing: Listing?){
            listing?.let {
                binding.listingPrice.text = listing.displayPrice
                binding.listingAddress.text = listing.address
                binding.listingBedbathcar.text = "${listing.features?.bedrooms} Bed  •  ${listing?.features?.bathrooms} Bath  •  ${listing?.features?.cars} Car"
                binding.listingShortlisted.isChecked = viewModel.isShortlisted(listing.id)
                binding.listingShortlisted.setOnCheckedChangeListener { button, isChecked ->
                    if (button.isPressed) {
                        viewModel.modifyShortList(isChecked, listing)
                        updateList()
                    }
                }
                val adapter = ListingsImagePagerAdapter(listing.images)
                binding.propertyImages.adapter = adapter
                binding.wormDotsIndicator.setViewPager(binding.propertyImages)
            }
        }
    }
}