package au.com.homely.takehomeexercise.ui.shortlist

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Adapter
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import au.com.homely.takehomeexercise.R
import au.com.homely.takehomeexercise.databinding.ShortlistFragmentBinding
import au.com.homely.takehomeexercise.ui.SharedViewModel
import au.com.homely.takehomeexercise.ui.listings.ListingRecyclerViewAdapter
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ShortlistFragment : Fragment() {

    private val sharedViewModel: SharedViewModel by activityViewModels()
    private var _binding: ShortlistFragmentBinding? = null
    private val binding get() = _binding!!
    private lateinit var adapter:ShortListRecyclerViewAdapter

    @SuppressLint("NotifyDataSetChanged")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = ShortlistFragmentBinding.inflate(inflater, container, false)
        adapter = ShortListRecyclerViewAdapter(sharedViewModel){
            adapter.notifyDataSetChanged()
        }
        binding.shortListingsListview.adapter = adapter
        return binding.root
    }
}
