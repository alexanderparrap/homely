package au.com.homely.takehomeexercise.ui.imageviewer

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import au.com.homely.takehomeexercise.databinding.ImageviewerFragmentBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ImageViewerFragment : Fragment() {

    private val viewModel: ImageViewerViewModel by activityViewModels()
    private var _binding: ImageviewerFragmentBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = ImageviewerFragmentBinding.inflate(inflater, container, false)

        viewModel.loadImage(requireContext()){
            binding.imageViewerImageview.setImageBitmap(it)
        }
        return binding.root
    }
}
