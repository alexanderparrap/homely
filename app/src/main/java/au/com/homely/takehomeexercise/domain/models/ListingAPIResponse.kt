package au.com.homely.takehomeexercise.domain.models

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ListingAPIResponse(
    val results: List<Listing>,
    val totalResults: Int
)

@JsonClass(generateAdapter = true)
data class Listing(
    val id: String,
    val rentalDetails: RentalDetails,
    val location: Location,
    val features: Features,
    val images: List<Image>
) {
    val address = location.address
    val displayPrice = rentalDetails.listingDetails.displayPrice

    @JsonClass(generateAdapter = true)
    data class RentalDetails(
        val listingDetails: ListingDetails
    )

    @JsonClass(generateAdapter = true)
    data class ListingDetails(
        val displayPrice: String?
    )

    @JsonClass(generateAdapter = true)
    data class Location(
        val address: String
    )

    @JsonClass(generateAdapter = true)
    data class Features(
        val bathrooms: Int,
        val bedrooms: Int,
        val cars: Int
    )
    @JsonClass(generateAdapter = true)
    data class Image(
        val uri: String,
    )
}
