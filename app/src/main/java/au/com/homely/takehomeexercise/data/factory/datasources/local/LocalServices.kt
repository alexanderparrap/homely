package au.com.homely.takehomeexercise.data.factory.datasources.local

import android.content.Context
import au.com.homely.takehomeexercise.domain.models.ListingAPIResponse
import com.squareup.moshi.Moshi
import java.io.IOException

const val LISTINGS_FILE = "listings.json"

class LocalServices(
    val context:Context
    ): DataLocalSource  {
    override fun retrieveListings(): ListingAPIResponse? {
        val jsonString = try {
            context.assets.open(LISTINGS_FILE).bufferedReader().use { it.readText() }
        } catch (ex: IOException) {
            ex.printStackTrace()
            return null
        }
        val builder = Moshi.Builder()
            .build()
        val adapter = builder.adapter(ListingAPIResponse::class.java)
        return adapter.fromJson(jsonString)
    }
}