package au.com.homely.takehomeexercise.ui.imageviewer

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class ImageViewerViewModel @Inject constructor() : ViewModel() {

    fun loadImage(context: Context, callback:(Bitmap)->Unit) {
        viewModelScope.launch {
            val imageStream = withContext(Dispatchers.IO) {
                context.assets.open("moon.jpg")
            }
            callback(withContext(Dispatchers.IO) {
                val bitmap = BitmapFactory.decodeStream(imageStream)
                Bitmap.createScaledBitmap(bitmap,400,240,true)
            })
        }
    }
}
