package au.com.homely.takehomeexercise.ui

import androidx.lifecycle.*
import au.com.homely.takehomeexercise.domain.models.Listing
import au.com.homely.takehomeexercise.domain.usecases.ListingsUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SharedViewModel@Inject constructor(
    private val listingsUseCase: ListingsUseCase
) : ViewModel() {
    lateinit var listings:List<Listing>
    private val _shortlist = MutableLiveData<MutableList<Listing>>().apply {
        value = mutableListOf()
    }
    val shortlist: LiveData<MutableList<Listing>> = _shortlist

    fun retrieveListings() {
        viewModelScope.launch {
            listings = if (listingsUseCase.retrieveListings() == null) {
                listOf()// Fixme: show a toast or alert dialog instead of show a empty list
            } else {
                listingsUseCase.removeDuplicateListings(listingsUseCase.retrieveListings()!!.results)
            }
        }
    }
    fun isShortlisted(id: String) = shortlist.value?.any { it.id == id } ?: false
    fun modifyShortList(isChecked: Boolean,listing: Listing){
        if(isChecked){
            shortlist.value?.add(listing)
        }else{
            shortlist.value?.remove(listing)
        }
    }
}